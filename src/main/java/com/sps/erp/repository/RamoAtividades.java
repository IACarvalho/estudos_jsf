package com.sps.erp.repository;

import java.util.List;

import com.sps.erp.model.RamoAtividade;

import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

public class RamoAtividades {
    
    private EntityManager manager;

    public RamoAtividades() {}

    public RamoAtividades(EntityManager manager) {
        this.manager = manager;
    }

    public List<RamoAtividade> pesquisar(String descricao) {
        CriteriaBuilder builder = manager.getCriteriaBuilder();

        CriteriaQuery<RamoAtividade> query = builder.createQuery(RamoAtividade.class);

        Root<RamoAtividade> root = query.from(RamoAtividade.class);
        query.select(root);
        query.where(builder.like(root.get("descricao"), descricao + "%"));
        
        return manager.createQuery(query).getResultList();
    }
}
