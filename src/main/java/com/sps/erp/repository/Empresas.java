package com.sps.erp.repository;

import java.util.List;

import com.sps.erp.model.Empresa;

import jakarta.persistence.EntityManager;

public class Empresas {

    private EntityManager manager;

    public Empresas() {}

    public Empresas(EntityManager manager) {
        this.manager = manager;
    }

    public Empresa porId(Long id) {
        return manager.find(Empresa.class, id);
    }

    public List<Empresa> pesquisar(String name) {
        String jpql = "from Empresa where upper(nomeFantasia) like :nomeFantasia";
        return manager.createQuery(jpql, Empresa.class)
                .setParameter("nomeFantasia", name.toUpperCase() + "%")
                .getResultList();
    }

    public Empresa guardar(Empresa empresa) {
        return manager.merge(empresa);
    }

    public void remover(Empresa empresa) {
        empresa = porId(empresa.getId());
        manager.remove(empresa);
    }
}
