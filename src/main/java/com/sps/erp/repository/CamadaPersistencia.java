package com.sps.erp.repository;

import java.util.Date;
import java.util.List;

import com.sps.erp.model.Empresa;
import com.sps.erp.model.RamoAtividade;
import com.sps.erp.model.TipoEmpresa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class CamadaPersistencia {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SpsPU");
        
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        // Declarando repositórios
        RamoAtividades ramoAtividades = new RamoAtividades(em);
        Empresas empresas = new Empresas(em);

        // Buscando as informações do banco
        List<RamoAtividade> listaRamoAtividades = ramoAtividades.pesquisar("");
        List<Empresa> listaEmpresas = empresas.pesquisar("");
        System.out.println(listaEmpresas);

        // Criando uma nova empresa
        Empresa empresa = new Empresa();
        empresa.setNomeFantasia("João da Silva");
        empresa.setCnpj("41.952.519/0001-57");
        empresa.setRazaoSocial("João da Silva 41952519000157");
        empresa.setTipo(TipoEmpresa.MEI);
        empresa.setDataFundacao(new Date());
        empresa.setRamoAtividade(listaRamoAtividades.get(0));

        // Salvando a emrpesa
        empresas.guardar(empresa);

        em.getTransaction().commit();

        // Verificando se a inserção funcionaou
        List<Empresa> listaEmpresas2 = empresas.pesquisar("");
        System.out.println(listaEmpresas2);

        em.close();
        emf.close();
    }
    
}
