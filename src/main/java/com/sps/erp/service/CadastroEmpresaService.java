package com.sps.erp.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.sps.erp.model.Empresa;
import com.sps.erp.repository.Empresas;
import com.sps.erp.util.Transacional;

public class CadastroEmpresaService implements Serializable{

    @Inject
    private Empresas empresas;

    @Transacional
    public void salvar(Empresa empresa) {
        empresas.guardar(empresa);
    }

    @Transacional
    public void excluir(Empresa empresa) {
        empresas.remover(empresa);
    }
    
}
